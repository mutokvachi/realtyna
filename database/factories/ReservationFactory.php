<?php

use Faker\Generator as Faker;
use App\Models\Reservation;

$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'room_id'   => rand(0, 15),
        'person'    => $faker->firstName,
        'date_from' => '2019-04-13',
        'date_to'   => '2019-04-25',
    ];
});
