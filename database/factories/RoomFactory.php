<?php

use Faker\Generator as Faker;
use App\Models\Room;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'image'       => $faker->imageUrl(),
        'intro'       => $faker->text(100),
        'description' => $faker->text(500),
    ];
});
