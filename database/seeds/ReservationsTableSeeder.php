<?php

use App\Models\Reservation;
use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{
    public function run()
    {
        factory(Reservation::class, 3)->create();
    }
}
