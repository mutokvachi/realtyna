<?php

use Illuminate\Database\Seeder;
use App\Models\Room;

class RoomsTableSeeder extends Seeder {
    public function run () {
        factory(Room::class, 30)->create();
    }
}
