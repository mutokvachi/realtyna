<?php

namespace App\Http\Requests;

use App\Rules\RoomAvailable;
use Illuminate\Foundation\Http\FormRequest;

class CreateReservationRequest extends FormRequest {
    public function authorize () {
        return true;
    }

    public function rules () {
        return [
            'person'    => 'required|min:4|max:30|regex:/^[\pL\s\-]+$/u',
            'date_from' => 'required|date_format:Y-m-d',
            'date_to'   => 'required|date_format:Y-m-d',
            'room_id'   => ['required', new RoomAvailable($this)],
        ];
    }
}
