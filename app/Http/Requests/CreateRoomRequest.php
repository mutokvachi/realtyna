<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoomRequest extends FormRequest {
    public function authorize () {
        return true;
    }

    public function rules () {
        return [
            'name'        => 'required|min:5|max:30|regex:/^[\pL\s\-]+$/u',
            'image'       => 'required|url',
            'intro'       => 'required|min:5|max:50',
            'description' => 'required',
        ];
    }
}
