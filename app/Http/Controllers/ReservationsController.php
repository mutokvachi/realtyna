<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReservationRequest;
use App\Models\Reservation;
use App\Models\Room;
use Illuminate\Http\Request;

class ReservationsController extends Controller {

    // http://dev.realtyna.ge/api/reservations
    public function index (Request $request) {
        return Reservation::with('room')->paginate(25);
    }

    // http://dev.realtyna.ge/api/reservations
    // body: person,date_from,date_to,room_id
    public function store (CreateReservationRequest $request) {
        $room = Room::findOrFail($request->get('room_id'));
        return [
            'status' => 'success',
            'data'   => $room->reservations()->create($request->all()),
        ];
    }

    // http://dev.realtyna.ge/api/reservations/1
    public function show ($id) {
        return Reservation::with('room')->findOrFail($id);
    }

}
