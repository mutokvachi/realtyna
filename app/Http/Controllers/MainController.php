<?php

namespace App\Http\Controllers;

use URL;
use App;
use Config;
use App\Models\Test;
use App\Helpers\Stuff;
use Illuminate\Http\Request;

class MainController extends Controller {
    public function index () {
        return view('user.index');
    }

    public function details(){
        return view('user.details');
    }
}
