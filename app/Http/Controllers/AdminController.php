<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use Validator;
use App\Models\Room;
use Illuminate\Http\Request;

class AdminController extends Controller {
    public function index () {
        return view('admin.index');
    }
}
