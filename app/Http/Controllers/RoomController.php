<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoomRequest;
use App\Models\Reservation;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller {

    // http://dev.realtyna.ge/api/rooms?date_from=2019-02-10&date_to=2019-02-15
    // http://dev.realtyna.ge/api/rooms
    public function index (Request $request) {
        if($request->date_from && $request->date_to){
            $reservedIds = Reservation::reservedRoomIds($request);

            if($reservedIds == 'error')
                return response()->json('Invalid date formats', 422);

            return Room::whereNotIn('id', $reservedIds)->paginate(15);
        }

        return Room::paginate(15);
    }

    // http://dev.realtyna.ge/api/rooms
    // post request body: name,image,intro,description
    public function store (CreateRoomRequest $request) {
        return Room::create($request->all());
    }

    // http://dev.realtyna.ge/api/rooms/1
    public function show ($id) {
        return Room::findOrFail($id);
    }
}
