<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {
    protected $dontReport = [
        //
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report (Exception $exception) {
        parent::report($exception);
    }

    public function render ($request, Exception $e) {
        if ($e instanceof ValidationException) {
            return response(['status' => 'error', 'errors' => $e->errors()], 422);
        }
        if($e instanceof ModelNotFoundException){
            return response(['status' => 'error', 'reason' => "Not found"], 404);
        }
        if($e instanceof NotFoundHttpException){
            return response(['status' => 'error', 'reason' => "Not found"], 404);
        }
        return parent::render($request, $e);
    }
}
