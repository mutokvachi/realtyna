<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {
    protected $fillable = ['name', 'image', 'intro', 'description'];

    // Relationships
    public function reservations () {
        return $this->hasMany(Reservation::class);
    }
}
