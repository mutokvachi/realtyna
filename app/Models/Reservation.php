<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {

    protected $fillable = ['person', 'room_id', 'date_from', 'date_to'];

    // Relationships
    public function room () {
        return $this->belongsTo(Room::class);
    }

    // methods
    public static function reservedRoomIds ($request) {
        $from = strtotime($request->date_from);
        $to   = strtotime($request->date_to);
        $days = ($to - $from) / 3600 / 24;

        if ($days <= 0) {
            return 'error';
        }

        return Reservation::
            where(function ($query) use ($request){
                $query->where('date_from', '<=', $request->date_from)
                    ->where('date_to', '>=', $request->date_from);
            })->orWhere(function ($query) use ($request){
                $query->where('date_from', '<=', $request->date_to)
                    ->where('date_to', '>=', $request->date_to);
            })->orWhere(function ($query) use ($request){
                $query->where('date_from', '>=', $request->date_from)
                    ->where('date_to', '<=', $request->date_to);
            })
            ->pluck('room_id');
    }

}
