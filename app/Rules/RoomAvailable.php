<?php

namespace App\Rules;

use App\Models\Reservation;
use App\Models\Room;
use Illuminate\Contracts\Validation\Rule;

class RoomAvailable implements Rule {
    public $request;

    public function __construct ($request) {
        $this->request = $request;

    }

    public function passes ($attribute, $value) {
        $room = Room::findOrFail($value);

        if(empty($this->request->date_from))
            return false;

        $reservedIds = Reservation::reservedRoomIds($this->request);

        if($reservedIds == 'error')
            return false;

        if(in_array($room->id, $reservedIds->all())){
            return false;
        }
        return true;
    }

    public function message () {
        return 'Room not available';
    }
}
