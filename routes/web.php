<?php

// web
Route::get('/', 'MainController@index')->name('index');
Route::get('/room/{id}', 'MainController@details')->name('details');


Route::group(['prefix' => 'admin'], function(){
	Route::get('/index', 'AdminController@index')->name('adminIndex');
});
