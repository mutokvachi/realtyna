<?php

// Rooms
Route::resource('rooms', 'RoomController');
// Reservations
Route::resource('reservations', 'ReservationsController');
