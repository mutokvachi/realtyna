function selectboxSelector(){
	var selector = $('.multi-select');
	for(var i = 0; i < selector.length; i++){
		var values = selector.eq(i).find('.hide-class').html();
		values = JSON.parse(values);

		for(var j in values){
			selector.eq(i).find("option[value="+values[j]+"]").attr('selected', true);
		}
	}
}

function translateRequiredValidation(){
	var k = document.querySelectorAll('[required]');
    for(var i = 0; i < k.length; i++){
        k[i].addEventListener('invalid', function(e){
            this.setCustomValidity('ველის შევსება აუცილებელია !');
        });
        k[i].addEventListener('change', function(e){
            try{this.setCustomValidity('')}catch(e){}
        });
    }
}

function alertBeforeDelete(){
	$('.delete-alert').click(function(){
		if(!confirm('წასაშლელად გთხოვთ დააჭიროთ "OK" '))
			return;

		var target = $(this).attr('delete-id');
        window.location.href = target;
	});
}

function selectPopOption(){
	var selects = $('.pop_option');
	for(var i = 0; i < selects.length; i++){
		var selector = selects.eq(i);
		var value = selector.attr('value');

		selector.attr('selected', true);
		selector.parent().find('option[value='+value+']:not(.pop_option)').remove();
	}
}

function runSummernote(){
	$('textarea').not('.nonSummernote').summernote({
		placeholder: 'შეავსეთ ველი',
		tabsize: 2,
		height: 100,
		toolbar: [
		    // [groupName, [list of button]]
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['color', ['color']],
		    ['insert', ['link']],
		  ]
	});
	$('textarea').each(function(key, item){
		$('textarea').eq(key).removeAttr('required');
	});
}

function alertErrors(errors){
	if(!errors)
		return;
	errors = JSON.parse(errors);	
	for(var i in errors){
		$("[name="+errors[i]+"]").closest('.form-group').find('.control-label').css('color','red');
	}
}

// function selectBeautifuler(){
// 	$('select').select2();
// }

$(document).ready(function(){
	selectboxSelector();
	translateRequiredValidation();
	alertBeforeDelete();
	selectPopOption();
	runSummernote();
	// selectBeautifuler();
});