@extends('user.layouts.master')

@section('head')
    @parent
@endsection

@section('banner')
    @parent
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 hotel-listing">
            {{--JS data here--}}
        </div>
    </div>

@endsection

@section('js')
    <script>
		function getRoomListing(){
			$.ajax({
				url:     'http://dev.realtyna.ge/api/rooms',
				method:  'GET',
				success: function (res) {
					for (var i in res.data) {
						$('.hotel-listing').append(`
						<div class="d-block d-md-flex listing-horizontal">
							<a href="/room/` + res.data[i].id + `" class="img d-block" style="background-image: url(` + res.data[i].image + `)">
							</a>
							<div class="lh-content">
								<h3><a href="/room/` + res.data[i].id + `">` + res.data[i].name + `</a></h3>
								<p>` + res.data[i].intro + `</p>
							</div>
						</div>
					`);
					}
				}
			})
		}
		getRoomListing();
    </script>
@endsection
