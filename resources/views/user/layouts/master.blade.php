<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hotel Realtyna</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('head')
</head>
<body>
@section('banner')
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('/images/hero_1.jpg');" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h1>Hotel Realtyna</h1>
                        <p class="mb-0">Make Your Reservation Here!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@show
<div class="site-section">
    <div class="container">
        @yield('content')
    </div>
</div>
<script src="{{ asset('plugins/inspinia/js/jquery-3.1.1.min.js') }}"></script>
@yield('js')
</body>
</html>
