@extends('user.layouts.master')

@section('head')
    @parent
@endsection

@section('banner')
    @parent
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="mb-5 border-bottom pb-5 room-details">
                <img src="" alt="Image" class="img-fluid mb-4">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <p class="room-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit numquam expedita, exercitationem harum magni natus, mollitia, similique itaque quasi, accusamus ipsa dignissimos eos. Porro in expedita rerum nam? Voluptas, porro!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 ml-auto">
            <div class="mb-5">
                <h3 class="h5 text-black mb-3">Reservation</h3>
                <form action="#" method="post">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" name="person" placeholder="John" class="form-control">
                    </div>
                    <input type="hidden" class="room-id" name="room_id">
                    <div class="form-group">
                        <label>Date From:</label>
                        <input type="date" name="date_from" placeholder="date" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Date To:</label>
                        <input type="date" name="date_to" placeholder="date" class="form-control">
                    </div>

                    <button class="btn btn-success btn-block" type="button">Reserve</button>
                    <div class="alert alert-success mt-2 text-center">Room reserved</div>
                    <div class="alert alert-danger mt-2 text-center">Room can't be reserved</div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function getSingleRoom(){
			$('.alert').hide();
        	var id = window.location.href.substr(window.location.href.lastIndexOf('/') +1);
			$.ajax({
				method: 'get',
				url: 'http://dev.realtyna.ge/api/rooms/'+id,
				success:function(res){
                    $('.room-details img').attr('src', res.image);
                    $('.room-desc').html(res.description);
					$('.room-id').val(res.id);
				}
			})
        }
        function reserveRoom(){
        	$('.alert').hide();
        	console.log($('form').serialize());
			$.ajax({
			    method: 'post',
			    url: 'http://dev.realtyna.ge/api/reservations',
			    data: $('form').serialize(),
			    success:function(res){
			    	if (res.status == 'success')
                        $('.alert-success').show();
			    },
                error: function () {
					$('.alert-danger').show();
				}
			})
		}

		function onReserveClick(){
			$(".btn-success").click(function () {
				reserveRoom();
			});
        }

		getSingleRoom();
		onReserveClick();
    </script>
@endsection
