<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link href="{{ asset('plugins/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/summernote/css/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/inspinia/css/style.css') }}" rel="stylesheet">
    @yield('head')
</head>
<body>
    <div id="wrapper">
        @section('nav')
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <span>
                                    <img alt="image" class="img-circle" src="{{ asset('plugins/inspinia/img/profile_small.jpg') }}" />
                                </span>
                            </div>
                            <div class="logo-element">
                                IN+
                            </div>
                        </li>
                        <li class="active">
                            <a href="index.html">
                                <i class="fa fa-th-large"></i>
                                <span class="nav-label">Main</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                {{--<li class="active-nav-test">--}}
                                    {{--<a href="route('roomView')">Rooms</a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        @show

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                </nav>
            </div>
            <div class="row">
                <div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('plugins/inspinia/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/select2/select2.full.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('plugins/inspinia/js/inspinia.js') }}"></script>
    <script src="{{ asset('plugins/inspinia/js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('/plugins/summernote/js/summernote.min.js') }}"></script>

    @yield('footer')

    @yield('javascript')
    <script type="text/javascript">
        var segment = window.location.pathname.split('/')[2];
        $('.active-nav-'+segment).addClass('active');

    </script>
</body>
</html>
