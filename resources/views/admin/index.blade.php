@extends('admin.layouts.master')

@section('head')

    <link href="{{ asset('plugins/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .dataTables_info,
        .dataTables_length,
        .pagination {
            display: none;
        }

        tbody tr {
            cursor: pointer;
        }
    </style>
@endsection

@section('nav')
    @parent
@endsection

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Reservations</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <th>ID</th>
                    <th>Room</th>
                    <th>Name</th>
                    <th>date from</th>
                    <th>date to</th>
                    </thead>
                    <tbody>
                        {{--js here--}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="d-none _token" token="{{csrf_token()}}"></div>
@endsection

@section('footer')
    <script src="{{ asset('plugins/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        function initDataTable(){
			$('.dataTables-example').DataTable({
				pageLength: 500,
				responsive: true,
				buttons:    [
					{
						extend:    'print',
						customize: function (win) {
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');

							$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
						}
					}
				]

			});
		}

		function getTableData(){
			$.ajax({
				url: 'http://dev.realtyna.ge/api/reservations',
				method:'GET',
				success:function (res) {
                    for (var i in res.data){
						$('tbody').append(`
                    	    <tr class="gradeX" data-target='`+res.data[i].id+`'>
                                <td>`+res.data[i].id+`</td>
                                <td>`+res.data[i].room.name+`</td>
                                <td>`+res.data[i].person+`</td>
                                <td>`+res.data[i].date_from+`</td>
                                <td>`+res.data[i].date_to+`</td>
                            </tr>
                    	`);
                    }
					initDataTable();
				}
			})
		}
		getTableData();
    </script>
@endsection
